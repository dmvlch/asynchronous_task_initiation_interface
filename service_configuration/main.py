from fastapi import FastAPI, HTTPException, Request
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from typing import List, Optional
import re
import time
import sys


sys.path.append("..")
from models.status_model import TaskStatus
from models.config_model import EquipmentConfig


app = FastAPI()


class EquipmentResponse(BaseModel):
    code: int
    message: str


@app.exception_handler(500)
def internal_exception_handler(request: Request, exc: Exception):
    return JSONResponse(
        status_code=500,
        content={"code": 500, "message": "Internal provisioning exception"},
    )


@app.post("/api/v1/equipment/cpe/{id}", response_model=EquipmentResponse)
def configure_equipment(id: str, config: EquipmentConfig):
    if not re.match(r"^[a-zA-Z0-9]{6,}$", id):
        raise HTTPException(
            status_code=404, detail="The requested equipment is not found"
        )

    time.sleep(60)
    print("success")
    return EquipmentResponse(code=200, message="success")
