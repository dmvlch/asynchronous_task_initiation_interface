from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from datetime import datetime

import sys
import os
import uuid
import pika
import re
import json
import sqlite3
import time

sys.path.append("..")
from models.status_model import TaskStatus
from models.config_model import EquipmentConfig
from models import config_model


app = FastAPI()

# Подключение к RabbitMQ
connection = pika.BlockingConnection(
    pika.ConnectionParameters(
        "localhost", heartbeat=600, blocked_connection_timeout=300
    )
)
channel = connection.channel()
channel.queue_declare(queue="task_queue")


@app.post("/api/v1/equipment/cpe/{id}")
def configure_equipment(id: str, config: config_model.EquipmentConfig):
    if not re.match(r"^[a-zA-Z0-9]{6,}$", id):
        raise HTTPException(
            status_code=404, detail="The requested equipment is not found"
        )

    task_id = str(uuid.uuid4())

    dt_object = datetime.fromtimestamp(time.time())
    formatted_time = dt_object.strftime("%d.%m.%Y-%H:%M:%S")

    message = {
        "timestamp": formatted_time,
        "equipment_id": id,
        "task_id": task_id,
        "config": config.dict(),
    }
    channel.basic_publish(
        exchange="", routing_key="task_queue", body=json.dumps(message)
    )

    with sqlite3.connect("tasks.db") as conn:
        cursor = conn.cursor()
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS tasks_performed (
                id INTEGER PRIMARY KEY,
                task_id TEXT NOT NULL,
                equipment_id TEXT NOT NULL
            )
            """
        )

        data = []
        data_tuple = tuple([task_id, id]) 
        data.append(data_tuple)

        cursor.executemany('INSERT INTO tasks_performed (task_id, equipment_id) VALUES (?, ?)', data)
        conn.commit()

    return TaskStatus(code=200, message=f"Task created with ID: {task_id}")


@app.get("/api/v1/equipment/cpe/{id}/task/{task_id}", response_model=TaskStatus)
def get_task_status(id: str, task_id: str):
    with sqlite3.connect("tasks.db") as conn:
        cursor = conn.cursor()
        cursor.execute(
            """
            CREATE TABLE IF NOT EXISTS completed_tasks (
                id INTEGER PRIMARY KEY,
                task_id TEXT NOT NULL,
                equipment_id TEXT NOT NULL
            );
            """
        )

        #получение данных из таблиц с завершенными задачами и задачами на выполнении
        '''
        cursor.execute(
            """
            SELECT * FROM completed_tasks;
            """
            )
        print(cursor.fetchall())
        
        cursor.execute(
            """
            SELECT * FROM tasks_performed;
            """
            )

        print(cursor.fetchall())
        '''

        cnt = channel.queue_declare(queue="result_queue", passive=True).method.message_count
        data = []
        if cnt:
            for count_massages in range(cnt):
                method_frame, properties, body = channel.basic_get("result_queue", auto_ack=True)
                str_data = json.loads(body.decode('utf-8'))

                data_tuple = tuple([str_data['task_id'], str_data['equipment_id']])
                
                data.append(data_tuple)

            cursor.executemany('INSERT INTO completed_tasks (task_id, equipment_id) VALUES (?, ?)', data)

            conn.commit()
            
            cursor.execute(
            """
            SELECT * FROM completed_tasks;
            """
            )

            #completed_tasks = cursor.fetchall()

        cursor.execute(
        """
        SELECT task_id, equipment_id  
            FROM completed_tasks
            WHERE task_id = ? AND equipment_id = ?;
        """, (task_id, id)
        )
            
        completed_tasks = cursor.fetchall()
        #print(completed_tasks)

        if len(completed_tasks) != 0:
            if len(completed_tasks[0]) == 2:
                print('Completed')
        elif len(completed_tasks) == 0:
            cursor.execute("SELECT EXISTS(SELECT 1 FROM completed_tasks WHERE equipment_id = ?)", (id,))
            equipment_exists = cursor.fetchone()[0]
            cursor.execute("SELECT EXISTS(SELECT 1 FROM completed_tasks WHERE task_id = ?)", (task_id,))
            task_exists = cursor.fetchone()[0]

            if task_exists and not equipment_exists:
                print("The requested equipment is not found")
            elif not task_exists and equipment_exists:
                print("The requested task is not found")
            else:
                print("Both task and equipment are found")

            #узнать какие задачи находятся в процесе выполнения
            cursor.execute(
                """
                DELETE FROM tasks_performed
                    WHERE (task_id, equipment_id) IN (
                        SELECT task_id, equipment_id FROM completed_tasks
                        EXCEPT
                        SELECT task_id, equipment_id FROM tasks_performed
                    );
                """
                )
            cursor.execute(
                """
                SELECT * FROM tasks_performed;
                """
                )

        print(cursor.fetchall())

        
    return TaskStatus(code=204, message="Task is still running")
