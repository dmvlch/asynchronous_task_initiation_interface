import sqlite3


with sqlite3.connect("tasks.db") as conn:
    cursor = conn.cursor()
    
    cursor.execute(
        """
        SELECT * FROM completed_tasks;
        """
        )
    print(cursor.fetchall())
        
    cursor.execute(
        """
        SELECT * FROM tasks_performed;
        """
        )

    print(cursor.fetchall())
    
    cursor.execute(
            """
            DELETE FROM tasks_performed
                WHERE (task_id, equipment_id) IN (
                    SELECT task_id, equipment_id FROM completed_tasks
                    EXCEPT
                    SELECT task_id, equipment_id FROM tasks_performed
                )
            """
            )

    print(cursor.fetchall())