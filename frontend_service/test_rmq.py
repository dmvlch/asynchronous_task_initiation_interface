import pika

# Параметры подключения
credentials = pika.PlainCredentials('guest', 'guest') # Замените 'username' и 'password' на ваши учетные данные
parameters = pika.ConnectionParameters('localhost', 5672, '/', credentials)

# Установка подключения
connection = pika.BlockingConnection(parameters)
print("Подключение к RabbitMQ установлено")

channel = connection.channel()

# Объявление очереди
channel.queue_declare(queue='task_queue', durable=True)

# Удаление очереди
channel.queue_delete(queue='task_queue')

# Закрытие подключения
connection.close()
print("Подключение к RabbitMQ закрыто")
