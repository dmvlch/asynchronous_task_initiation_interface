import concurrent.futures
import pika
import json
import requests


def callback(channel, ch, method, properties, body):
    message = body.decode("utf-8")
    task = json.loads(message)
    timestamp = task["timestamp"]
    equipment_id = task["equipment_id"]
    task_id = task["task_id"]
    config = task["config"]

    response = requests.post(
        f"http://localhost:8000/api/v1/equipment/cpe/{equipment_id}", json=config
    )

    if response.status_code == 200:
        result = {
            "timestamp": timestamp,
            "task_id": task_id,
            "equipment_id": equipment_id,
            "config": config,
        }
        channel.queue_declare(queue="result_queue")
        channel.basic_publish(
            exchange="", routing_key="result_queue", body=json.dumps(result)
        )
        channel.basic_ack(delivery_tag=method.delivery_tag)
    else:
        print(f"Request failed with status code {response.status_code}")

def start_consumer(queue_name):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            "localhost", heartbeat=600, blocked_connection_timeout=300
        )
    )
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)

    callback_with_channel = lambda ch, method, properties, body: callback(
        channel, ch, method, properties, body
    )

    channel.basic_consume(
        queue=queue_name, on_message_callback=callback_with_channel, auto_ack=False
    )
    print(f"Started consumer for {queue_name}")
    channel.start_consuming()

# Создаем пул потоков с 5 потоками
with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    for _ in range(5):
        executor.submit(start_consumer, "task_queue")
