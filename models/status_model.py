from pydantic import BaseModel

class TaskStatus(BaseModel):
    code: int
    message: str
