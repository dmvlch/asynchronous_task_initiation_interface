from pydantic import BaseModel
from typing import List, Optional


class Parameters(BaseModel):
    username: str
    password: str
    vlan: Optional[int] = None
    interfaces: List[int]


class EquipmentConfig(BaseModel):
    timeoutInSeconds: int
    parameters: Parameters